import { createStore } from 'redux';

// Action generators: fns which returns action objects

// Also eg. of destructuring the argument
const incrementCount = ({ incrementBy = 1 } = {}) => {
    return {
        type: 'increment',
        incrementBy
    };
};

store.dispatch(incrementCount({ incrementBy: 5 }));

//expect fn as arg
//second arg is action
// store is what tracks changing data
// when we call the createStore() the function we pass gets called immediately
const store = createStore((state = { count: 0 }, action) => {
    if (action.type === "increment") {
        return {
            count: state.count + action.incrementBy
        };
    } else {
        return state;
    }
});

// return current state
store.getState();


// Actions: Change the state of the redux store
// It is nothing more but an object which gets sent to the store
// increment Count

store.dispatch({
    type: "increment"
});


// How to watch for store state change
// Gets called everytime when the store changes
const Unsubscribe = store.subscribe(() => {
    console.log(store.getState());
});

// Unsubsribe from store updates
Unsubscribe();

//----------------------- Object Destructuring -----------------------
const person =  {
    name: 'Mohit',
    age: '25',
    location: {
        city: 'udr',
        temp: '35'
    }
};

// Instead of this
console.log(`${person.name} is ${person.age}`);

// we can do this and 
// set default values as well
const {name = 'Anonymous', age} = person;

//renaming the variable while destructuring
const {city, temp: temperature} = person.location;

console.log(`${name} is ${age}`);
console.log(`${city} is ${temperature}`);

//----------------------- Array Destructuring -----------------------
const address = ['h7404 pfs', 'bangalore', 'KR', '313205'];

// only destrucutre the required items
// You can set the default values as well
const [, cityy, state = 'Default City'] = address;

// Instead of doing this
console.log(`You are in ${cityy} ${state}`);


// ---------------------- Reducer ------------------------//
// Determine what happens with state when the action is triggered
// Reducers are pure functions, where output only depends on input
// It won't use any variable outside function's scope or modify an outside scope
// Never directly change state or action

const countReducer = (state = { count: 0 }, action) => {
    if (action.type === "increment") {
        return {
            count: state.count + action.incrementBy
        };
    } 
    else {
        return state;
    }
};

const reduxStore = createStore(countReducer);

reduxStore.getState();
