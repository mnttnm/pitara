import { createStore, combineReducers } from 'redux';


// sample link Object
const sampleState = {
    category: "youtube",
    description: "Speaking of Sex",
    id: 5,
    img: "http://dummyimage.com/128x128.png/5fa2dd/ffffff",
    rating: 3,
    tags: "Comedy|Romance",
    url: "http://tiny.cc/nulla/tellus/in/sagittis/dui/vel/nisl.html?et=non&ultrices=interdum&posuere=in&cubilia=ante&curae=vestibulum&duis=ante&faucibus=ipsum&accumsan=primis&odio=in&curabitur=faucibus&convallis=orci&duis=luctus&consequat=et&dui=ultrices&nec=posuere&nisi=cubilia&volutpat=curae&eleifend=duis&donec=faucibus&ut=accumsan&dolor=odio&morbi=curabitur&vel=convallis&lectus=duis&in=consequat&quam=dui&fringilla=nec&rhoncus=nisi&mauris=volutpat&enim=eleifend&leo=donec&rhoncus=ut&sed=dolor&vestibulum=morbi&sit=vel&amet=lectus&cursus=in&id=quam&turpis=fringilla&integer=rhoncus&aliquet=mauris&massa=enim&id=leo&lobortis=rhoncus&convallis=sed&tortor=vestibulum&risus=sit&dapibus=amet&augue=cursus&vel=id&accumsan=turpis&tellus=integer&nisi=aliquet&eu=massa&orci=id&mauris=lobortis&lacinia=convallis&sapien=tortor&quis=risus"
    ,
    views: 302,
    _id: "5b2f430b3c368154f1234aaf"
};

// Store creation
store.subscribe(() => {
    console.log(store.getState());
});