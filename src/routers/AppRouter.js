import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import React from 'react';

import PageNotFound from '../components/PageNotFound';
import CategoryList from '../components/CategoryList';
import LinkList from '../components/LinkList';
import TestComponent from '../components/TestComponent';

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <div className="header">
                <Link className="title" to="/"> DeepStambh </Link>
                <p> My version of awesome list </p>
            </div>

            <Switch>
                <Route path="/" exact={true} component={CategoryList} />
                <Route path="/test" component={TestComponent} />
                <Route path="/categories" component={CategoryList} exact={true} />
                <Route path="/categories/:category" component={LinkList} />
                <Route component={PageNotFound} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;