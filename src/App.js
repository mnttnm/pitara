import React, { Component } from 'react';
import 'normalize.css/normalize.css';
import './styles/App.css';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
// provider provides store to all the components.
import { Provider } from 'react-redux';

const store = configureStore();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <AppRouter />
            </Provider>
        );
    }
}

export default App;
