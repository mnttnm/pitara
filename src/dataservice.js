import request from 'superagent';

const dataService = store => next => action => {
    /*
  Pass all actions through by default
  */
    next(action);
    switch (action.type) {
    case 'GET_CATEGORIES':
        /*
    In case we receive an action to send an API request, send the appropriate request
    */
        request.get('http://127.0.0.1:3000/pitara/categories').end((err, res) => {
            if (err) {
                /*
          in case there is any error, dispatch an action containing the error
          */
                return next({
                    type: 'GET_TODO_DATA_ERROR',
                    err
                });
            }
            const data = JSON.parse(res.text);
            /*
        Once data is received, dispatch an action telling the application
        that data was received successfully, along with the parsed data
        */
            next({
                type: 'GET_CATEGORIES_RECEIVED',
                data
            });
        });
        break;
        /*
      Do nothing if the action does not interest us
      */
    case 'GET_LINKS_FOR_CATEGORY':
        request.get('http://127.0.0.1:3000/pitara/links/' + action.category)
            .end((err, res) => {
                if (err) {
                    /*
                      in case there is any error, dispatch an action containing the error
                      */
                    return next({
                        type: 'GET_LINKS_ERROR',
                        err
                    });
                }
                const data = JSON.parse(res.text);
                /*
                    Once data is received, dispatch an action telling the application
                    that data was received successfully, along with the parsed data
                    */
                next({
                    type: 'GET_LINKS_RECEIVED',
                    data: data.links,
                    tags: action.tags
                });
            });
        break;
    case 'GET_TAGS_FOR_CATEGORY':
        break;
    default:
        break;
    }
};

export default dataService;