// SORT_BY_VIEWS
export const sortByViews = () => ({
    type: 'SORT_BY_VIEWS',
    sortBy: "views"
});

// SORT_BY_LIKES
export const sortByLikes = () => ({
    type: 'SORT_BY_LIKES',
    sortBy: "likes"
});

// SORT_BY_NAME
export const sortByName = () => ({
    type: 'SORT_BY_NAME',
    sortBy: "name"
});

// SORT_BY_DATE_CREATED
export const sortByDateCreated = () => ({
    type: 'SORT_BY_DATE_CREATED',
    sortBy: "date"
});

// SET_TAG_FILTER
export const setTagFilter = (tags = []) => ({
    type: 'SET_TAG_FILTER',
    tags
});