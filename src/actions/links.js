// Get Categories
export const getCategories = () => ({
    type: 'GET_CATEGORIES'
});

// GET_LINKS_FOR_CATEGORY
export const getLinksForCategory = (category) => ({
    type: 'GET_LINKS_FOR_CATEGORY',
    category
});

// GET_TAGS_FOR_CATEGORY
export const getTagsForCategory = (category) => ({
    type: 'GET_TAGS_FOR_CATEGORY',
    category
});