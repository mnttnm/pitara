// get filtered links

export default (links, { tags, sortBy }) => {
    console.log(`tags: ${tags}, sortBy: ${sortBy}`);
    return links.filter((link) => {
        if(tags.length > 0) {
            const tagsArray = link.tags.split("|");
            let isTagsPresent = false;
            for(let i = 0; i < tags.length; i++) {
                if(tagsArray.includes(tags[i])) {
                    isTagsPresent = true;
                }
            }
            return isTagsPresent;
        }
        return true;
    }).sort((a, b) => {
        switch (sortBy) {
        case 'views':
            return a.views < b.views ? 1 : -1;
        case 'likes':
            return a.rating < b.rating ? 1 : -1;
        case 'name':
            return a.description < b.description ? -1 : 1;
        case 'date':
            return a.date < b.date ? 1 : -1;
        default:
            return a.views < b.views ? 1 : -1;
        }
    });
};