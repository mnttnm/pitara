// Filters Reducer
const filterReducerDefaultState = {
    text: '',
    sortBy: 'views', // ratings, date-added
    tags: ['Action', 'Comedy'],
};

export default (state = filterReducerDefaultState, action) => {
    switch (action.type) {
    case "SORT_BY_VIEWS":
    case "SORT_BY_LIKES":
    case "SORT_BY_NAME":
    case "SORT_BY_DATE_CREATED":
        return { ...state, sortBy:action.sortBy};
    case "SET_TAG_FILTER":
        return { ...state, tags: action.tags };
    default:
        return state;
    }
};