// Link Reducer

const categoryReducerDefaultState = {
    categories: [],
    error: "",
    isLoaded:false
};

export default (state = categoryReducerDefaultState, action) => {
    switch (action.type) {
    case "GET_CATEGORIES_RECEIVED":
        console.log("GET_CATEGORIES_RECEIVED");
        return {...state, categories: action.data.categories, isLoaded: true};
    case "GET_CATEGORIES_ERROR":
        return {...state, isLoaded: true, error:action.err};
    default:
        return state;
    }
};