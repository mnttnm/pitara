// Link Reducer

const linkReducerDefaultState = {
    links: [],
    error: "",
    isLoaded:false,
    tags: ""
};

export default (state = linkReducerDefaultState, action) => {
    switch (action.type) {
    case "GET_LINKS_RECEIVED":
        return {...state, links:action.data, tags:action.data.tags, isLoaded:true};
    case "GET_LINKS_ERROR":
        return {...state, isLoaded: true, error:action.err};
    default:
        return state;
    }
};