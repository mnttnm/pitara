import { createStore, combineReducers, applyMiddleware } from 'redux';
import linkReducer from '../reducers/links';
import filterReducer from '../reducers/filters';
import categoryReducer from '../reducers/category';
import dataService from '../dataservice';

export default () => {
    const store = createStore(
        combineReducers({
            links: linkReducer,
            filters: filterReducer,
            categories: categoryReducer
        }), 
        applyMiddleware(dataService)
    );
    return store;
};
