import React from 'react';
import {Link} from 'react-router-dom';

const Category = (props) => {
    return(
        <div className="category">
            <Link className="cat-link" to={"/categories/" + props.category.toLowerCase()}> {props.category} </Link>
        </div>
    );
};

export default Category;