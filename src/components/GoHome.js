import React from 'react';
import {Link} from 'react-router-dom';

const GoHome = () => (
    <div>
        <Link to="/">Home</Link>
    </div>
);

export default GoHome;