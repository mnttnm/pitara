import React, { Component } from 'react';
import GoHome from '../components/GoHome';
import { connect } from 'react-redux';
import { getLinksForCategory, getTagsForCategory } from '../actions/links';
import LinkListFilters from '../components/LinkListFilters';
import getFilteredLinksForCategory from '../selectors/links';
import CategoryTags from  '../components/CategoryTags';


function NoLinks() {
    return <h2 > No links for this Category </h2>;
}

class LinkList extends Component {
    constructor() {
        super();
        this.getSelectedCategory.bind(this);
        this.getTagsForSelectedCategory.bind(this);
    }

    getSelectedCategory(){
        var selectedCat = this.props.categories.filter((category) => {
            if(category.name.toLowerCase() === this.props.match.params.category.toLowerCase()) {
                return true;
            }
            return false;
        });
        return selectedCat[0];
    }

    getTagsForSelectedCategory() {
        var selectedCategory = this.getSelectedCategory();
        var tagsArray = selectedCategory.tags.split("|");
        console.log("Tags for Current Category: " + JSON.stringify(tagsArray));
        return tagsArray;
    }

    componentDidMount() {
        console.log("Fetching links for " + this.props.match.params.category);
        this.props.dispatch(getLinksForCategory(this.props.match.params.category));
    }

    Display(links) {
        if(links.length > 0) {
            return links.map(link => (
                <a className="linkItem"
                    key={link.description}
                    href={link.url}> {link.description}
                </a>
            ));
        } else {
            return <NoLinks/>;
        }

    }

    render() {
        const { links, error, isLoaded } = this.props;
        if (!isLoaded && error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            var tagsArray = this.getTagsForSelectedCategory();
            return (
                <div className="links-container">
                    <div className="linkListHeader">
                        <GoHome />
                        <CategoryTags tags={tagsArray}/>
                        <LinkListFilters />
                    </div>
                    <div className="links">
                        {
                            this.Display(links)
                        }
                    </div>
                </div >
            );
        }
    }
}


// // argument of connect defines 
// // what information do we need from store
// // connect returns a function

// const ConnectedLinksList = connect((state) => {
//     // whatever we'll return from here
//     // will be accessible as props in the component
//     return {
//         name: state.filters.sortBy
//     };
// })(LinkList);



// // Common pattern
// const mapStateToProps


// export default ConnectedLinksList;

// this will automatically get updated as
// when the state will change in the store
const mapStateToProps = (state) => {
    console.log("links");
    console.log(state.links.links);
    console.log("filters");
    console.log(state.filters);
    console.log('categories');
    console.log(state.categories);
    return {
        links: getFilteredLinksForCategory(state.links.links, state.filters),
        isLoaded: state.links.isLoaded,
        error: state.links.error,
        categories: state.categories.categories
    };
};

// connect returns a function, where we need to provide our component
// for which we need a higher order component
export default connect(mapStateToProps)(LinkList);