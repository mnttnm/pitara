import React, { Component } from 'react';
import Category from './Category';
import { connect } from 'react-redux';
import { getCategories } from '../actions/links';

//const store = configureStore();

class CategoryList extends Component {
    componentDidMount() {
        this.props.dispatch(getCategories());
    }

    render() {
        const { error, isLoaded, categories } = this.props;
        if (isLoaded && error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="category-container">
                    {categories.map(category => {
                        var catName = category.name.replace(/\w/, c => c.toUpperCase());
                        return (<Category
                            tags={category.tags}
                            key={category.name}
                            category={catName}
                        />);
                    })}
                </div>
            );
        }
    }
}

const mapStatetoProps = (state, dispatch) => {
    console.log(state);
    return {
        categories: state.categories.categories,
        isLoaded: state.categories.isLoaded,
        error: state.categories.error,
        dispatch
    };
};

export default connect(mapStatetoProps)(CategoryList);