import React from 'react';
import {Link} from 'react-router-dom';

const CategoryTags = (props) => {
    return (
        <div>
            {
                props.tags.map(tag => 
                {
                    return(
                        <Link to="#"> {tag} </Link>
                    );                  
                })
            }
        </div>
    );
};

export default CategoryTags;