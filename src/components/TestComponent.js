import React from 'react';
import GoHome from '../components/GoHome';

const TestComponent = () => (
    <div>
        <p> This is Test Component </p>
        <GoHome />
    </div>
);

export default TestComponent;


