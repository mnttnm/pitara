import React from 'react';
import GoHome from '../components/GoHome';

const PageNotFound = () => (
    <div>
        404 Page not Found!
        <GoHome />
    </div>
);

export default PageNotFound;