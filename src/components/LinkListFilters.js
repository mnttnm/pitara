import React from 'react';
import {connect} from 'react-redux';
import {sortByViews, sortByName, sortByDateCreated, sortByLikes} from '../actions/filters';

const LinkListFilters = (props) =>  (
    <div>
        <select value={props.filters.sortBy} onChange={(e) => {
            switch(e.target.value) {
            case "views":
                props.dispatch(sortByViews());
                break;
            case "name":
                props.dispatch(sortByName());
                break;
            case "likes":
                props.dispatch(sortByLikes());
                break;
            case "date":
                props.dispatch(sortByDateCreated());
                break;
            default:
                props.dispatch(sortByLikes());
                break;
            }
        }}
        >
            <option value="views"> Views </option>
            <option value="name"> Name </option>
            <option value="date"> Date </option>
            <option value="likes"> Likes </option>
        </select>
    </div>
);

const mapStateToProps = (state) => {
    return  {
        filters: state.filters
    };
};

export default connect(mapStateToProps)(LinkListFilters);